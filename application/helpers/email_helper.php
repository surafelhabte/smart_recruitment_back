<?php

function email($FirstName, $email, $Link, $subject)
{
    $ci=&get_instance();
    $connected = @fsockopen("mail.appdiv.com", 587);
    if ($connected) {
        $data = ['fullName'=>$FirstName,'Link'=>$Link];
        $message = $ci->load->view('password_reset_email_template_view', $data, true);
        $ci->load->config('email');
        $ci->load->library('email');
        $ci->email->from(config_item('smtp_user'), 'Ethiopian Commodity Exchange');
        $ci->email->to($email);
        $ci->email->subject($subject);
        
        $ci->email->message($message);
        $sent= $ci->email->send();
        if ($sent) {
            return $sent;
        }
    } else {
        $is_conn = false;
    }
    return false;
}

function Verification_email($FirstName, $email, $Link, $subject)
{
    $ci=&get_instance();
    $connected = @fsockopen("mail.appdiv.com", 587);
    if ($connected) {
        $data = ['fullName' => $FirstName,'Link' => $Link];
        $message = $ci->load->view('signup_confirmation_email_template_view', $data, true);
        $ci->load->config('email');
        $ci->load->library('email');
        $ci->email->from(config_item('smtp_user'), 'Ethiopian Commodity Exchange');
        $ci->email->to($email);
        $ci->email->subject($subject);
        
        $ci->email->message($message);
        $sent= $ci->email->send();
        if ($sent) {
            return $sent;
        }
    } else {
        $is_conn = false;
    }
    return false;
}


