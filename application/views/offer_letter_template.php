<p><span style="color: #1f497d;">D</span>ear ( <span style="color: #ff0000;"><?php echo $fullName ?></span> ),</p>
<p align="justify"><span style="font-size: medium;">
We are very pleased to inform you that our selection panel has selected you for the position of <span style="color: #ff0000;">
<strong>(<?php echo $Position ?>)</strong></span> Branch.</p> 
<p>See the <strong>job offer</strong> below.We believe you would agree with our offer and join our team.</p>
<p><strong>Tell us the ideal date for you to begin your work and please we would be glad if you join us as early as possible</strong>.</span></p>
<table border="1" width="660" cellspacing="0" cellpadding="2">
<tbody>
    <tr valign="top">
    <td style="background: #e8e8e8;" bgcolor="#e8e8e8" width="219" height="17">
    <p align="center"><span style="color: #000000;"><strong>Benefit Types</strong></span></p>
    </td>
    <td style="background: #e8e8e8;" bgcolor="#e8e8e8" width="411">
    <p align="center"><span style="color: #000000;"><strong>Benefit Package</strong></span></p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p>Salary</p>
    </td>
    <td width="411">
    <p>Birr <span style="color: #ff0000;">Salary</span> (Gross)</p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p>Transportation Allowance</p>
    </td>
    <td width="411">
    <p>Birr</p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p>Mobile Allowance</p>
    </td>
    <td width="411">
    <p>Birr</p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p>Medical Expenses Coverage</p>
    </td>
    <td width="411">
    <p>Birr 5,000/year per staff, spouse and children</p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p>GPA Insurance</p>
    </td>
    <td width="411">
    <p>24 Hours&rsquo; Insurance Coverage for staff only</p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p>Bonus and Annual Merit Increase</p>
    </td>
    <td width="411">
    <p>Based on annual company performance and individual performance evaluation</p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p><span style="color: #000000;">Pension Fund &ndash; 18%</span></p>
    </td>
    <td width="411">
    <p><span style="color: #000000;">11% and 7% by the Exchange and staff respectively</span></p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p><span style="color: #000000;">Annual Leave Entitlement</span></p>
    </td>
    <td width="411">
    <p><span style="color: #000000;">Begins with 18 working days per year to a maximum of 23 working days</span></p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p><span style="color: #000000;">&nbsp;Staff Loan</span></p>
    </td>
    <td width="411">
    <p>2 up to 6 months&rsquo; <span style="color: #000000;">salary to be repaid within </span>12 up to 36 months as per HR procedure</p>
    </td>
    </tr>
    <tr valign="top">
    <td width="219">
    <p><span style="color: #000000;">Credit Association</span></p>
    </td>
    <td width="411">
    <p><span style="color: #000000;">Voluntary membership</span></p>
    </td>
    </tr>
</tbody>
</table>
<p><span style="color: #000000;"> Please also make sure to bring the below list of requirements on your arrival. </span></p>
<table border="1" width="660" cellspacing="0" cellpadding="2">
<tbody>
    <tr>
    <td style="background: #e8e8e8;" colspan="2" bgcolor="#e8e8e8" width="644" height="12">
    <p align="center"><span style="color: #000000;">&nbsp;</span><span style="color: #000000;"><strong>New Employee Document Requirements</strong></span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">1</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Copy and Original documents of </span><span style="color: #000000;"><strong>Academic credentials (degree, diploma&hellip;.)</strong></span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">2</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Copy and Original documents of </span><span style="color: #000000;"><strong>Release (Clearance) paper from previous employer</strong></span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">3</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Copy and Original documents of </span><span style="color: #000000;"><strong>Work experience from previous employer</strong></span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">4</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Copy and Original documents of </span><span style="color: #000000;"><strong>Marriage certificate</strong></span><span style="color: #000000;"> (if any)</span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">5</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Copy and Original documents of </span><span style="color: #000000;"><strong>Birth Certificate</strong></span><span style="color: #000000;"> ( if any)</span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">6</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Copy and Original documents of </span><span style="color: #000000;"><strong>Cost sharing </strong></span><span style="color: #000000;">( if any)</span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">7</span></p>
    </td>
    <td width="573">
    <p>5 passport size <strong>photographs</strong></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">8</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">1 passport size </span><span style="color: #000000;"><strong>photographs of children</strong></span><span style="color: #000000;"> ( if any)</span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">9</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">1 passport size </span><span style="color: #000000;"><strong>photographs of Spouse </strong></span><span style="color: #000000;">( if any)</span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">10</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Copy and Original of all documents </span><span style="color: #000000;"><strong>written on your CV </strong></span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center"><span style="color: #000000;">11</span></p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">Forensic and Medical Certificate (ECX support letter will be provided at the starting date of your employment)</span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="13">
    <p align="center">12</p>
    </td>
    <td width="573">
    <p><span style="color: #000000;">TIN Number</span></p>
    </td>
    </tr>
    <tr>
    <td valign="bottom" width="57" height="12">
    <p align="center">1<span style="color: #1f497d;">3</span></p>
    </td>
    <td width="573">
    <p>Dashen Bank/CBE/United Bank/Awash Bank <strong>Account Number </strong></p>
    </td>
    </tr>
</tbody>
</table>
<p class="western">&nbsp;</p>