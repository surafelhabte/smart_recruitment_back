<?php 
   Class Application_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
      } 
      
    public function Update_Application($post) {
      $this->db->trans_begin();
      $this->db->update_batch('Application', $post, 'id');
     
      if($this->db->trans_status() === true)
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'Application Updated Successfully.'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'Unable to Update Application'];
      }

    } 
  
    public function Get_Applications_First($vac_id) {
      $result = $this->db->select('Vacancy_Type')->get_where('Vacancy',['id'=>$vac_id])->row()->Vacancy_Type;

      if($result === 'Internal'){
        return $this->db->select('first_name as FirstName,middle_name as Surname,sex As Gender,date(app.created_date) As Date,vac.Title,
                                  vac.Created_date,app.id,IFNULL(app.Shortlist,"Not-Completed") as Shortlist,app.First,app.Second,vac.Current_Step')
                        ->from('Application as app')
                        ->order_by('app.created_date', 'DESC')
                        ->where("Shortlist is NULL AND VacancyId = '$vac_id'")
                        ->join("$this->core_Db.employee_data as emp","emp.employee_id = app.Employee_id")
                        ->join('Vacancy as vac', 'vac.id = app.VacancyId')
                        ->get()->result_array();
      } else {
        return $this->db->select('Photo,FirstName,Surname,Gender,date(app.created_date) As Date,vac.Title,
                                 vac.Created_date,app.id,IFNULL(app.Shortlist,"Not-Completed") as Shortlist,app.First,app.Second,vac.Current_Step')
                        ->from('Application as app')
                        ->order_by('app.created_date', 'DESC')
                        ->where("Shortlist is NULL AND VacancyId = '$vac_id'")
                        ->join('Applicant as appli', 'appli.id = app.Applicant_Id')
                        ->join('Vacancy as vac', 'vac.id = app.VacancyId')
                        ->get()->result_array();
      }

    }

    public function Get_Applications_Second($post) {
      $result = $this->db->select('Vacancy_Type')->get_where('Vacancy',['id'=>$post['vac_id']])->row()->Vacancy_Type;

      if ($result === 'Internal') {
        return $this->db->select('first_name as FirstName,middle_name as Surname,sex As Gender,date(app.created_date) As Date,vac.Title,vac.Created_date,app.id,
                                  IFNULL(app.Shortlist,"Not-Completed") as Shortlist,app.First,app.Second,vac.Current_Step')
                        ->from('Application as app')
                        ->order_by('app.created_date', 'DESC')
                        ->where("Shortlist is NULL AND VacancyId = '$post[vac_id]'")
                        ->join("$this->core_Db.employee_data as emp","emp.employee_id = app.Employee_id")
                        ->join('Vacancy as vac', 'vac.id = app.VacancyId')
                        ->join("Jobs as jo", "jo.Department = '$post[dep_id]'")
                        ->get()->result_array();
        
      } else {
        return $this->db->select('Photo,FirstName,Surname,Gender,date(app.created_date) As Date,vac.Title,vac.Created_date,app.id,
                                 IFNULL(app.Shortlist,"Not-Completed") as Shortlist,app.First,app.Second,vac.Current_Step')
                        ->from('Application as app')
                        ->order_by('app.created_date', 'DESC')
                        ->where("Shortlist is NULL AND VacancyId = '$post[vac_id]'")
                        ->join('Applicant as appli', 'appli.id = app.Applicant_Id')
                        ->join('Vacancy as vac', 'vac.id = app.VacancyId')
                        ->join("Jobs as jo", "jo.Department = '$post[dep_id]'")
                        ->get()->result_array();
      }

    }

    public function Get_Application($id) {
      $result = $this->db->select('Vacancy_Type')->from('Application as app')->where(['app.id'=>$id])
                         ->join('Vacancy as vac', 'vac.id = app.VacancyId')
                         ->get()->row()->Vacancy_Type;

      if ($result === 'Internal') {
          $select = 'app.id,app.Title,app.Employee_id,CONCAT(first_name," ",middle_name) as full_name,location,
                      position,department_name,sex,date_of_birth as Dob,con.email,con.mobile,app.Created_Date';

          $complete['bio_data'] = $this->db->select($select)
                                          ->from('Application as app')
                                          ->where('app.id', $id)
                                          ->join("$this->core_Db.employee_data as emp", "emp.employee_id = app.Employee_id")
                                          ->join("$this->core_Db.contact_info as con", "emp.employee_id = con.employee_id")
                                          ->join("$this->core_Db.position as pos", "emp.position_id = pos.id")
                                          ->join("$this->core_Db.department as dep", "pos.department_id = dep.id")
                                          ->get()->row();

                                          
          $lpd = $this->db->order_by('to_date')
                          ->select('IFNULL(to_date,"Not Promoted") as lpd')
                          ->get_where("$this->core_Db.employee_status",['employee_id'=>$complete['bio_data']->Employee_id])->row(); 

          $complete['bio_data']->lpd = $lpd;

                                                 
          $complete['Qualifications'] = $this->db->select('certificate,start_date,end_date,school,subject')
                                                  ->get_where("$this->core_Db.education as edu", 
                                                     ["edu.employee_id" => $complete['bio_data']->Employee_id])
                                                  ->result_array();

          $complete['Experiences'] = $this->db->select('place_of_work,from_date,to_date,position')
                                              ->get_where("$this->core_Db.experience as exp", 
                                                  ["exp.employee_id" => $complete['bio_data']->Employee_id])
                                              ->result_array();
          $complete['vacancy'] = $result;

          return $complete;

      } else {
          $select = 'Application.id,Application.Title,CoverLetter,Document,Application.Applicant_Id,FirstName,Surname,
                   Gender,Dob,Disability,Email,Phone,Photo,CoverLetter,Application.Created_Date,CVs.id As CVId';
  
          $this->db->select($select)->from('Application')
                  ->where('Application.id', $id)
                  ->join('CVs', 'Application.CVId = CVs.id')
                  ->join('Applicant', 'Applicant.id = Application.Applicant_Id');
  
          $complete['bio_data'] = $this->db->get()->row();
          $complete['Qualifications'] = $this->db->get_where('Qualifications', ['CVId' => $complete['bio_data']->CVId])->result_array();
          $complete['Experiences'] = $this->db->get_where('Experiences', ['CVId' => $complete['bio_data']->CVId])->result_array();
          $complete['References'] = $this->db->get_where('References', ['CVId' => $complete['bio_data']->CVId])->result_array();
          $complete['Trainings'] = $this->db->get_where('Trainings', ['CVId' => $complete['bio_data']->CVId])->result_array();
          $complete['vacancy'] = $result;
    
          return  $complete;
      }

    }
 
    public function Get_Vacancies() {
      $date = date('Y-m-d');
      return $this->db->select('CONCAT(vac.Title," => ",date(Created_date), " (",vac.Vacancy_Type,")") As text,vac.id As value,vac.Created_date As date')
                      ->from('Vacancy as vac')
                      ->where("vac.Current_Step < 1 AND vac.dead_line < '$date'")
                      ->join('Jobs as jo', 'jo.id = vac.JobId')      
                      ->get()->result_array();
    }
 
    public function Get_First_Shortlisted_Applications($vacancy_id) {
      $this->db->order_by('Application.created_date', 'ASC');
      $this->db->select('Photo,FirstName,Surname,Gender,Application.created_date As Date, Vacancy.Title, Vacancy.Created_date As post_date, 
      Application.id, Application.Status, Application.Shortlist');
      $this->db->from('Application');
      $this->db->where("Shortlist = 'First' AND Status != 'Failed' AND VacancyId=" . $vacancy_id);
      $this->db->join('Applicant', 'Applicant.id = Application.Applicant_Id');
      $this->db->join('Vacancy', 'Vacancy.id = Application.VacancyId');
      $result = $this->db->get()->result_array();
      return $result;
    }

    public function Get_Comments($id){
      return $this->db->select('CONCAT(emp.first_name," ",emp.middle_name) as comment_by,application_id,body,com.created_date')
                          ->from("Comments as com")
                          ->order_by('com.created_date','DESC')
                          ->where(['com.application_id'=>$id])
                          ->join("$this->core_Db.employee_data as emp","emp.employee_id = com.comment_by")
                          ->get()->result_array();
    }

    public function Post_Comment($post){
      $this->db->trans_begin();
      $this->db->insert('Comments', $post);
   
      if($this->db->trans_status() === true)
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'Remark posted successfully'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to post remark.'];

      }
    }

    
  } 