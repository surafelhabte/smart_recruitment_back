<?php 
   Class Panel_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
      } 
      
    public function Create($post) {
      $members = $post['members'];
      unset($post['members']); 

      foreach($members as &$value){
        $value = array_merge($value,$post);
        unset($value['id']);
      }

      $this->db->trans_begin();
      $this->db->insert_batch('Panel_Members', $members);

      if($this->db->trans_status() === true)
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'panel assigned successfully.'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to assign.'];
      }

    } 

    public function Update($post) {
        $members = $post['members'];
        unset($post['members']); 

        $data_without_id = [];

        foreach($members as $key => &$value){
          $value = array_merge($value,$post);

          if(empty($value['id'])){
            array_push($data_without_id,$value);
            array_splice($members,$key,1);
          }

        }
  
        $this->db->trans_begin();
        $this->db->update_batch('Panel_Members', $members,'id');

        if(count($data_without_id) > 0){
            $this->db->insert_batch('Panel_Members',$data_without_id);
        }

        if($this->db->trans_status() === true)
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'panel assignment updated successfully.'];
  
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to update.'];
        }
  
    }

    public function Gets(){
        return $this->db->select('pan.id,CONCAT(vac.Title," => ",date(vac.created_date)) as Title,
                                sch.AssesementFor as Step,count(pan.employee_id) as total_members, 
                                date(pan.created_date) as date, pan.vacancy_id,pan.step')
                        ->from('Panel_Members as pan')
                        ->join('Vacancy as vac','vac.id = pan.vacancy_id')
                        ->join('AssessmentSchedule as sch','sch.VacancyId = pan.vacancy_id AND sch.Step = vac.Current_Step')
                        // ->join('AssessmentProcedure as ass','ass.Assessment = sch.AssesementFor AND ass.Step = sch.Step')
                        ->group_by(['pan.vacancy_id','pan.step'])
                        ->get()->result_array();
    }

    public function Get($id){
        return $this->db->select('pan2.id,pan2.vacancy_id,pan2.step,pan2.employee_id as value,CONCAT(first_name," ",middle_name) As text')
                        ->from('Panel_Members as pan')
                        ->where(['pan.id' =>$id])
                        ->join('Panel_Members as pan2','pan.vacancy_id = pan2.vacancy_id AND pan.step = pan2.step')
                        ->join("$this->core_Db.employee_data as emp",'pan2.employee_id = emp.employee_id')
                        ->get()->result_array();
    }

    public function Delete($post){
        $this->db->trans_begin();
        $this->db->delete('Panel_Members', $post);
  
        if($this->db->trans_status() === true)
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'panel assignment deleted successfully.'];
  
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to delete assignment.'];

        }  
    }

    public function Remove($id){
        $this->db->trans_begin();
        $this->db->delete('Panel_Members', ['id' => $id]);
  
        if($this->db->trans_status() === true)
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'panel member removed successfully.'];
  
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to remove member.'];

        }  
    }

    public function Get_Vacancies() {
      return $this->db->select('CONCAT(vac.Title," => ",date(vac.Created_date)) as text,vac.id as value')
                      ->from('Vacancy as vac')
                      ->join('Panel_Members as pan','pan.vacancy_id != vac.id AND pan.step != vac.Current_Step','left')
                      ->join('AssessmentSchedule as sch','sch.VacancyId = vac.id AND sch.Step = vac.Current_Step')
                      ->get()->result_array();

    }

    public function Get_Steps($vac_id) {
        return $this->db->select('sch.Step as value,sch.AssesementFor as text')
                        ->from('AssessmentSchedule as sch')
                        ->where(['sch.VacancyId'=>$vac_id])
                        ->join('Vacancy as vac',"vac.id = $vac_id AND vac.Current_Step <= sch.Step")
                        ->get()->result_array();

    }

    public function Get_Panel_Members($post) {
        return $this->db->select('CONCAT(first_name," ",middle_name) as full_name, pos.position,dep.department_name')
                        ->from("Panel_Members as pan")
                        ->where($post)
                        ->join("$this->core_Db.employee_data as emp","pan.employee_id = emp.employee_id")
                        ->join("$this->core_Db.position as pos","emp.position_id = pos.id")
                        ->join("$this->core_Db.department as dep","pos.department_id = dep.id")
                        ->get()->result_array();
    } 

    public function Get_Employees($post) {
        return $this->db->select('employee_id as value,CONCAT(first_name," ",middle_name) As text')
                        ->from("$this->core_Db.employee_data")
                        ->group_start()
                            ->or_like('first_name',$post['keyword'])
                            ->or_like('middle_name',$post['keyword'])
                        ->group_end()
                        ->where_not_in('employee_id',$post['emp_id'])
                        ->get()->result_array();
    } 

  } 