<?php 
   Class Recruitment_Request_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function Create_Request($data) {
      $this->db->trans_begin();
      
      $this->db->set($data);
      if($this->db->insert('RecruitmentRequest'))
      {
        $this->db->trans_commit();          
        return ['status'=>true,'message'=>'Request Sent Successfully !'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Request not Sent'];
      } 
    }

    public function Update_Request($data) {
      $this->db->trans_begin();

      $this->db->where('id', $data['id']);
      $this->db->set($data);
     
      if($this->db->update('RecruitmentRequest'))
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Request updated successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Request not updated.'];
      }
    } 
  
    public function Get_Requests() {
      return $this->db->select('id, Division,Position,TypeOfEmployment,RequestedBy,Status,date(created_date) as created_date')
                      ->order_by('created_date', 'DESC')
                      ->get('RecruitmentRequest')->result_array();
      
    }

    public function Get_Request($id) {
      return $this->db->select('Division,Position,NumberOfRequired,JobGrade,TypeOfEmployment,StartingDate,RequestedBy,created_date')
                      ->get_where('RecruitmentRequest', ['id' => $id])->row();
    }
        
    public function Delete_Request($id) {
      $this->db->trans_begin();

      if($this->db->delete('RecruitmentRequest', array('id' => $id)))
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'Request Deleted successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to Delete Request.'];
      }
    } 
  } 