<?php 
   Class Recruit_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      }  

    public function Load_Applications($id){
      $this->db->select('FirstName,Surname,Gender,Photo,Application.id,Application.Title,
                        Vacancy.id As Vacancy_Id, Vacancy.Created_date As post_date,IFNULL(Hiring_Status,"In-Process") as Hiring_Status');
      $this->db->from('Application');
      $this->db->where("Application.VacancyId",$id);
      $this->db->join('Applicant', 'Applicant.id = Application.Applicant_Id AND Application.Status = "Pass" AND Application.Hiring_Status = "Offerd"'); 
      $this->db->join('Vacancy', 'Application.VacancyId = Vacancy.id');      
      $applications = $this->db->get()->result_array();
      return ['status'=>true,'message'=>(Object)['applications' => $applications]];
    } 
    
    public function Save($data){  
      $data['Applications'] = json_decode($data['Applications'], true);
      $this->db->update('Application',$data['Applications'],['id'=> $data['Application']['id']]);   
      
      $this->db->select('NumberOfRequired');
      $number_of_required = ($this->db->get_where('Vacancy', array('id' => $data['Vacancy_Id']))->row(0))->NumberOfRequired;
      $number_of_hired = count($this->db->get_where('Application',array('VacancyId' => $data['Vacancy_Id'], 'Hiring_Status' => 'Hired'))->result_array());

      if($number_of_required == $number_of_hired){
        $this->db->where('id', $data['Vacancy_Id']);
        $this->db->set(['Hired' => 'True']);
        $this->db->update('Vacancy');
      }
      
      if($this->db->trans_status() === true){
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Application Hiring Status updated Successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to update Application Hiring Status.'];
      }  
    }     
    
    public function Get_Vacancies() {
      return $this->db->select('CONCAT(vac.Title," => ",date(Created_date)) As text,vac.id As value,vac.Created_date As date')
                      ->from('Vacancy as vac')
                      ->where('vac.Current_Step is not null')
                      ->join('Jobs as jo', 'jo.id = vac.JobId')
                      ->join('AssessmentProcedure as ass', 'jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High AND (ass.Step + 1) = vac.Current_Step')
                      ->get()->result_array();
    }

  } 