<?php 
   Class Vacancy_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function Create_Vacancy($data) {
      $this->db->trans_begin();
      $this->db->set($data);
      if($this->db->insert('Vacancy'))
      {
        $this->db->trans_commit();          
        return ['status'=>true,'message'=>'Vacancy Created Successfully !'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Vacancy Not Created'];
      } 
    }

    public function Update_Vacancy($data) {
      $this->db->trans_begin();

      $this->db->where('id', $data['id']);
      $this->db->set($data);
     
      if($this->db->update('Vacancy'))
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Vacancy updated successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to Update Vacancy.'];
      }
    } 
  
    public function Get_Vacancies() {
      $result = $this->db->get('Vacancy')->result_array();
      return $result;
    }

    public function Get_Vacancy($id) {
      $result = $this->db->get_where('Vacancy', array('id' => $id))->row();
      return $result;
    }

    public function Delete_Vacancy($id) {
      $this->db->trans_begin();

        if($this->db->delete('Vacancy', array('id' => $id)))
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'Vacancy Deleted successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to Delete Vacancy.'];
        }
    }    
    
    public function Get_Vacancies_Have_Nextstep() {
      return $this->db->select('vac.Title As text,vac.id As value,vac.Current_Step,vac.Created_date As date, ass.panel_members_required as panel')
                      ->from('Vacancy as vac')
                      ->where('vac.Current_Step is not null')
                      ->join('Jobs as jo', 'jo.id = vac.JobId')
                      ->join('AssessmentProcedure as ass', 'jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High AND ass.Step = vac.Current_Step')
                      ->get()->result_array();
    }

    
  } 