<?php 
   Class Assessment_Schedule_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
      } 
      
    public function Create_Schedule($data) {
      $result = $this->db->from('AssessmentSchedule')->where(['VacancyId'=>$data['VacancyId'],'Step'=>$data['Step']])->count_all_results();
      
      if($result > 0){
        return ['status'=>false,'message'=>'Schedule Already Created'];

      } else {
        $this->db->trans_begin();
        $this->db->insert('AssessmentSchedule',$data);
  
        if($data['SendSchedule'] == 'True'){
          $this->prepare_message($data, $this->db->insert_id());
        }
  
        if($this->db->trans_status() === true)
        {
          $this->db->trans_commit();          
          return ['status'=>true,'message'=>'Schedule Created Successfully !'];

        } else {
          $this->db->trans_rollback();
          return ['status'=>false,'message'=>'Schedule Not Created'];

        } 
      }  

    }

    public function Update_Schedule($data) {
      $this->db->trans_begin();

      $this->db->delete('Message_Scheduler', array('Message_For_id' => $data['id']));
      $this->db->update('AssessmentSchedule',$data, array('id' => $data['id']));

      if($data['SendSchedule'] == 'True'){
        $this->prepare_message($data, $data['id']);
      }

      if($this->db->trans_status() === true)
      {
        $this->db->trans_commit();          
        return ['status'=>true,'message'=>'Schedule Updated Successfully !'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Schedule Not Updated'];
      } 
    }

    public function Get_Schedules() {
     return $this->db->select('AssessmentSchedule.id,AssessmentSchedule.Title,Vacancy.Title As VacancyTitle,Date,AssesementFor,SendSchedule')
                    ->order_by('Date DESC')
                    ->from('AssessmentSchedule')
                    ->join('Vacancy', 'Vacancy.id = AssessmentSchedule.VacancyId')
                    ->get()->result_array();
    }

    public function Get_Current_Step($vacancyId) {
      return $this->db->select('Assessment,ass.Step')
                      ->from('Vacancy as vac')
                      ->where(['vac.id'=> $vacancyId])
                      ->join('Jobs as jo', 'jo.id = vac.JobId')
                      ->join('AssessmentProcedure as ass', '(jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High) AND (ass.Step = vac.Current_Step)')
                      ->get()->row();
    }

    public function Get_Schedule($id) {
      return $this->db->get_where('AssessmentSchedule', ['id' => $id])->row();
    }

    public function Delete_Schedule($id) {
      $this->db->trans_begin();
      $this->db->delete('AssessmentSchedule', array('id' => $id));
      $this->db->delete('Message_Scheduler', array('Message_For_id' => $id));

      if($this->db->trans_status() === true)
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'Schedule Deleted successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to Delete Schedule.'];
      }
    }  
    
    private function prepare_message($data,$schedule_id){
      $this->load->library('Message_Scheduler');
      $Email_array = [];

      $Emails = $this->db->select('Email, Vacancy.Title')
               ->from('Vacancy')
               ->where('Vacancy.id', $data['VacancyId'])
               ->join('Application', 'Application.VacancyId = Vacancy.id AND Application.Status = "Pass"')
               ->join('CVs', 'CVs.id = Application.CVId')
               ->join('Applicant', 'CVs.Applicant_Id = Applicant.id')
               ->get()->result_array();

      foreach($Emails as $email){
        array_push($Email_array,$email['Email']);
      }

      if(count($Email_array) > 0){
        $this->message_scheduler->schedule('Schedule Notification', 
        'Dear Candidate, '. $data['AssesementFor'] . ' For ' . $Emails[0]['Title'] . ' Position will be held @ ' . 
        $data['Date'] = date("F j, Y")
        . ' On ' . $data['AssesementLocation'], 
        json_encode($Email_array, true), $schedule_id);
      }
    }

    public function Check_Panel_Members($data) {
      return $this->db->select('CONCAT(emp.first_name, "  ",emp.middle_name," => ", pos.position) as member')
                      ->from('Panel_Members as pan')
                      ->where($data)
                      ->join("$this->core_Db.employee_data as emp","pan.employee_id = emp.employee_id")
                      ->join("$this->core_Db.position as pos","emp.position_id = pos.id")
                      ->get()->result();

    }
    
  } 