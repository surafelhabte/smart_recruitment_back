<?php 
   Class Assessment_Procedure_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function Create_Procedure($data) {
      $this->db->trans_begin();
      
      $this->db->set($data);
      if($this->db->insert('AssessmentProcedure'))
      {
        $this->db->trans_commit();          
        return ['status'=>true,'message'=>'Procedure Created Successfully !'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Procedure Not Created'];
      } 
    }

    public function Update_Procedure($data) {
      $this->db->trans_begin();

      $this->db->where('id', $data['id']);
      $this->db->set($data);
     
      if($this->db->update('AssessmentProcedure'))
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Procedure updated successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Procedure Not updated.'];
      }
    } 
  
    public function Get_Procedures() {
      return $this->db->select('id,CONCAT(Job_Grade_Low," - ",Job_Grade_High) as JobGrade,Step,Assessment,Internal_Min_Point, 
                                External_Min_Point,Affirmative_Min_Point')
                      ->order_by('Job_Grade_Low ASC, Step ASC')
                      ->get('AssessmentProcedure')->result_array();
    }

    public function Get_Procedure($id) {
      return $this->db->get_where('AssessmentProcedure', ['id' => $id])->result_array();
      
    }

    public function Delete_Procedure($id) {
      $this->db->trans_begin();

        if($this->db->delete('AssessmentProcedure',['id' => $id]))
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'Procedure Deleted successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to Delete Procedure.'];
        }
    }        
     

    //for internal 
    public function Create_Internal_Procedure($data) {
      $this->db->trans_begin();
      $this->db->insert('Assessment_Procedure_Internal',$data);

      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Procedure Created Successfully.'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to create Procedure.'];

      }
      
    }

    public function Update_Internal_Procedure($data) {
      $this->db->trans_begin();
      $this->db->update('Assessment_Procedure_Internal',$data,['id'=>$data['id']]);

      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Procedure Updated Successfully.'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to update Procedure.'];

      }
      
    }

    public function Get_Internal_Procedures(){
      return $this->db->get('Assessment_Procedure_Internal')->result_array();
    }

    public function Get_Internal_Procedure($id){
      return $this->db->get_where('Assessment_Procedure_Internal',['id' => $id])->row();
    }

    public function Delete_Internal_Procedure($id) {
      $this->db->trans_begin();
      $this->db->delete('Assessment_Procedure_Internal',['id' => $id]);

      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Procedure Deleted Successfully.'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to delete Procedure.'];

      }
      
    }


    //For Setting
    public function Get_Setting(){
      return $this->db->get('Setting')->row();
    }

    public function Save_Setting($data) {
      $this->db->trans_begin();

      if(is_null($data['id'])){
        $this->db->insert('Setting',$data);

      } else {
        $this->db->update('Setting',$data,['id'=>$data['id']]);
      }

      
      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Setting Saved Successfully.'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to save Setting.'];

      }
      
    }

  } 