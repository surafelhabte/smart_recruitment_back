<?php 
   Class Job_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
      } 
      
    public function Create_Job($data) {
      $this->db->trans_begin();
      $this->db->set($data['JobRequirment']);
      if($this->db->insert('Jobs'))
      {
        $id = $this->db->insert_id();
        $QualificationAndExperience = 
        count($data['QualificationAndExperience']) > 0 ? $this->convertToArray($data['QualificationAndExperience'], $id) : [];

        if(count($QualificationAndExperience) > 0){
          $this->db->insert_batch('Qualification_And_Experience', $QualificationAndExperience['data_with_id']);
        }
        
        if($this->db->trans_status() === TRUE){
          $this->db->trans_commit();
          return ['status'=>true,'message'=>'Job created successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false,'message'=>'unable to create Job.'];
        }
      }
    }

    public function Update_Job($data) {
      $this->db->trans_begin();
      $this->db->where('id', $data['JobRequirment']['id']);
      $this->db->set($data['JobRequirment']);
     
      if($this->db->update('Jobs'))
      {
        $QualificationAndExperience = count($data['QualificationAndExperience']) > 0 ? $this->convertToArray($data['QualificationAndExperience'],
        $data['JobRequirment']['id'], true) : [];

        if(count($QualificationAndExperience) > 0){
          if(count($QualificationAndExperience['data_with_id']) > 0){
            $this->db->update_batch('Qualification_And_Experience',$QualificationAndExperience['data_with_id'],'id');
          }
          if(count($QualificationAndExperience['data_without_id']) > 0){
            $this->db->insert_batch('Qualification_And_Experience', $QualificationAndExperience['data_without_id']);
          }
        }
        
        if($this->db->trans_status() === TRUE){
          $this->db->trans_commit();
          return ['status'=>true,'message'=>'Job updated successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false,'message'=>'Unable to update Job.'];
        }
      } 
    } 
  
    public function Get_Jobs() {
      $jobs = $this->db->select('jo.id as JobId,jo.Position,Division,Field_Of_Study,Work_Experience,dep.department_name as Department,Sector,jo.JobGrade')
                       ->from('Jobs as jo') 
                       ->join("$this->core_Db.department as dep","dep.id = jo.Department")
                       ->get()->result_array();
      
      foreach($jobs as &$value) {
        $value['Field_Of_Study'] = json_decode($value['Field_Of_Study'], true);
        $value['Work_Experience'] = json_decode($value['Work_Experience'], true);
        $value['Sector'] = json_decode($value['Sector'], true);

        $QaE = $this->db->get_where('Qualification_And_Experience', ['JobId' => $value['JobId']])->result_array();
        $value['QualificationAndExperience'] = $QaE;
      }

      return $jobs;
    }

    public function Get_Job($id) {
      $result = $this->db->get_where('Jobs', array('id' => $id))->result_array();
      $result[0]['Field_Of_Study'] = json_decode($result[0]['Field_Of_Study'], true);
      $result[0]['Work_Experience'] = json_decode($result[0]['Work_Experience'], true);
      $result[0]['Sector'] = json_decode($result[0]['Sector'], true);

      $QaE = $this->db->get_where('Qualification_And_Experience', array('JobId' => $result[0]['id']))->result_array();
      $result[0]['QualificationAndExperience'] = $QaE;
      return $result;
    }

    public function Delete_Job($data) {
      $this->db->trans_begin();
      if($this->db->delete($data['table'], array('id' => $data['id'])))
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'Item Deleted successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to Delete Item.'];
      }
    }     
    
    public function convertToArray($data, $id, $forUpdate = false) {
      $data_without_id = array();
      $Converted_Data = $data;

      foreach($Converted_Data as $key => &$c_data) {
        $c_data['JobId'] = $id;
        if($forUpdate){
          if(empty($c_data['id'])){
            array_push($data_without_id, $c_data);
            array_splice($Converted_Data,$key,1);
          }
        }
      }
      return ['data_with_id' => $Converted_Data, 'data_without_id' => $data_without_id];
    }

    public function Get_Position_Info($post){
      return $this->db->select('pos.id as pos_id,pos.position,pos.job_grade,pos.department_id as dep_id,dep.department_name as dep_name')
                      ->from("$this->core_Db.position as pos")
                      ->like('pos.position',$post['keyword'])
                      ->join("$this->core_Db.department as dep",'pos.department_id = dep.id')
                      ->get()->result_array();
    }

  } 