<?php 
   Class Report_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function intialize_reports($data) {
      return $data;
    }
      
    public function Save_Report($data){
      $this->db->trans_begin();
      $this->db->set($data);
      if($this->db->insert('saved_reports'))
      {
        $this->db->trans_commit();          
        return ['status'=>true,'message'=>'Report Saved Successfully !'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to Save Report.'];
      } 
    }
    
    public function Get_Saved_Reports(){
      $result = $this->db->get('saved_reports')->result_array();
      return $result;
    }

    public function Delete_Saved_Report($id) {
      $this->db->trans_begin();

        if($this->db->delete('saved_reports', array('id' => $id)))
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'report Deleted successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to Delete report.'];
        }
    }

    public function Update_Saved_Report($data) {
      $this->db->trans_begin();
      $this->db->where('id', $data['id']);
      $this->db->set($data);
      if($this->db->update('saved_reports'))
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Report updated successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to update Report.'];
      }
    } 
  } 