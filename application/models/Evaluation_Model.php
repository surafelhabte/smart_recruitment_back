<?php 
   Class Evaluation_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
      } 

    public function Save_Result($post) {
      $fail = [];
      $pass = [];
      $hasPanelMembers = false;
      $proc = $this->get_proc($post['Vacancy_Id'], $post['Step']);
      $summery['Vacancy_Id'] = $post['Vacancy_Id'];
      $summery['Application_Id'] = $post['application_id'];

      if(is_array($post['Data'])){
        $hasPanelMembers = true;
        $summery['Result'] = 0;

        foreach($post['Data'] as $value){
          $summery['Result'] += $value['value'];
        }
      } else {
        $summery['Result'] = $post['Data'];

      }

      if($post['Vacancy_Type'] === 'Internal'){
        if($post['Gender'] === 'Female'){
          if($summery['Result'] >= $proc->int || ($summery['Result'] + $proc->aff) >= $proc->int){
            $summery['Assessment_Type'] = $proc->ass;
            $this->isCompleted($post['Vacancy_Id']) ? array_push($pass, ['id' => $summery['Application_Id'], 'Status' => 'Pass']) : '';

          } else {
            $summery['Assessment_Type'] = $proc->ass;
            array_push($fail, ['id' => $summery['Application_Id'], 'Status' => 'Failed']);

          }
        } else {
          if($summery['Result'] >=  $proc->int){
            $summery['Assessment_Type'] = $proc->ass;
            $this->isCompleted($post['Vacancy_Id']) ? array_push($pass, ['id' => $summery['Application_Id'], 'Status' => 'Pass']) : '';

          } else {
            $summery['Assessment_Type'] = $proc->ass;
            array_push($fail, ['id' => $summery['Application_Id'], 'Status' => 'Failed']);

          }
        }
      } else {
        if($post['Gender'] === 'Female'){
          if($summery['Result'] >= $proc->ext || ($summery['Result'] + $proc->aff) >= $proc->ext){
            $this->isCompleted($post['Vacancy_Id']) ? array_push($pass, ['id' => $summery['Application_Id'], 'Status' => 'Pass']) : '';
            $summery['Assessment_Type'] = $proc->ass;

          } else {
            $summery['Assessment_Type'] = $proc->ass;
            array_push($fail, ['id' => $summery['Application_Id'], 'Status' => 'Failed']);

          }
        } else {
          if($summery['Result'] >=  $proc->ext){
            $summery['Assessment_Type'] = $proc->ass;
            $this->isCompleted($post['Vacancy_Id']) ? array_push($pass, ['id' => $summery['Application_Id'], 'Status' => 'Pass']) : '';

          } else {
            $summery['Assessment_Type'] = $proc->ass;
            array_push($fail, ['id' => $summery['Application_Id'], 'Status' => 'Failed']);

          }
        }
      }

      $this->db->trans_begin();

      $this->db->insert('AssessmentSummery', $summery);

      count($fail) > 0 ? $this->db->update_batch('Application',$fail,'id') : [];
      count($pass) > 0 ? $this->db->update_batch('Application',$pass,'id') : [];
      $hasPanelMembers ? $this->db->insert_batch('Evaluation_Panel_Member',$post['Data']) : '';

      //check if the other remaining applications is available for evaluation
      if($this->hasRemaining($post['Vacancy_Id'],$post['Step'],$post['application_id'])){
        $this->db->update('Vacancy',['Current_Step' => ($proc->Step + 1)],['id' => $post['Vacancy_Id']]);

      }

      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Evaluation Result Saved successfully.'];

      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to Save Evaluation Result.'];

      }
    } 
  
    public function Load_Applications($id){
      $step = $this->db->select('Current_Step')->get_where('Vacancy',['id'=>$id])->row()->Current_Step;
      $proc = $this->get_proc($id,$step);

      if($proc){
        $proc = (Object)$proc;
          $applications = $this->db->select('FirstName,Surname,Gender,Photo,app.id,vac.Title,vac.id As Vacancy_Id,
                                        vac.Vacancy_Type, vac.Created_date As post_date,Current_Step as Step,IFNULL(panel_members_required,0) as panel')
                                    ->from('Application as app')
                                    ->where("app.Status = 'Inprocess' AND Shortlist = 'Completed' AND vac.dead_line < '". date('Y-m-d'). "' AND app.VacancyId=" . $id)
                                    ->join('Vacancy as vac', 'vac.id = app.VacancyId')
                                    ->join('Jobs as jo', 'vac.JobId = jo.id')
                                    ->join('AssessmentProcedure as ass','(jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High) AND
                                            vac.Current_Step = ass.Step')

                                    ->join('AssessmentSchedule as sch','vac.id = sch.VacancyId AND vac.Current_Step = sch.Step')

                                    ->join('CVs', 'CVs.id = app.CVId')
                                    ->join('Applicant as appl', 'CVs.Applicant_Id = appl.id')
                                    ->get()->result_array();

          return ['status'=>true,'message'=>(Object)['applications' => $applications,'procedure' => $proc]];

      } else {
        return ['status'=>false,'message'=>'Procedure not Available or Assessment Completed.
                Please Create Assessment Procedure for this vacancy on this Step then try again.'];

      } 
    } 

    private function get_proc($vacancy_id, $step = 0){
      return $this->db->select('Step,Assessment as ass,Internal_Min_Point as int,External_Min_Point as ext,Affirmative_Min_Point as aff,vac.Title')
                      ->from('Vacancy as vac')
                      ->where('vac.id', $vacancy_id)
                      ->join('Jobs as jo', 'jo.id = vac.JobId')
                      ->join('AssessmentProcedure as ass', "(jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High) AND ass.Step = $step")
                      ->get()->row();
    }

    public function Get_Vacancies() {
      return $this->db->select('CONCAT(vac.Title," => ",date(Created_date)) As text,vac.id As value,vac.Created_date As date')
                      ->from('Vacancy as vac')
                      ->where('vac.Current_Step > 0')
                      ->join('Jobs as jo', 'jo.id = vac.JobId')
                      ->join('AssessmentProcedure as ass', 'jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High AND ass.Step = vac.Current_Step')
                      ->get()->result_array();
    }

    public function Get_Panel_Members($post){
      return $this->db->select('CONCAT(first_name," ",middle_name) as text, pan.id as value')
                      ->from("Panel_Members as pan")
                      ->where($post)
                      ->join("$this->core_Db.employee_data as emp","pan.employee_id = emp.employee_id")
                      ->get()->result_array();
    }

    private function isCompleted($vac_id){
      $result = $this->db->from('Vacancy as vac')
                         ->where(['vac.id'=>$vac_id])
                         ->join('Jobs as jo','jo.id = vac.JobId')
                         ->join('AssessmentProcedure as ass','(jo.JobGrade between ass.Job_Grade_Low and Job_Grade_High)
                             AND ass.Step = (vac.Current_Step + 1)')
                         ->count_all_results();

      if($result === 0){
        return true;

      } else {
         return false;

      }   

    }

    private function hasRemaining($vac_id,$step,$app_id){
      $result = $this->db->from('Application as app')
                         ->where(['app.VacancyId'=>$vac_id,'app.Status'=>'InProcess','app.id !=' =>$app_id])
                         ->join('Vacancy as vac',"vac.id = app.VacancyId AND vac.Current_Step = $step")
                         ->count_all_results();

      if($result === 0){
        return true;

      } else {
        return false;

      }


    }

  } 