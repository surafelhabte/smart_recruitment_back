<?php 
   Class Offering_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 

    public function ShowResult($id){
      $applications = 
      $this->db->select('FirstName,Surname,Gender,Email,Photo,AssessmentSummery.Application_Id,Application.Title,Vacancy.id As Vacancy_Id,
                        Vacancy.Created_date As post_date, Application.Hiring_Status')
              ->group_by('Application_Id')
              ->select_sum('AssessmentSummery.Result')
              ->from('AssessmentSummery')
              ->where("AssessmentSummery.Vacancy_Id",$id)
              ->join('Application', 'Application.VacancyId = AssessmentSummery.Vacancy_Id AND Application.Status = "InProcess" AND 
                      Application.id = AssessmentSummery.Application_Id AND (Application.Hiring_Status is null OR Application.Hiring_Status = "Reserved")')
              ->join('Vacancy', 'Vacancy.id = AssessmentSummery.Vacancy_Id')
              ->join('Applicant', 'Applicant.id = Application.Applicant_Id')
              ->order_by('AssessmentSummery.Result', 'DESC')
              ->get()->result_array();

      return ['status'=>true,'message'=>(Object)['applications' => $applications]];
    } 
    
    public function ShowDetail($Application_Id){
      return $this->db->select('Assessment_Type, Result, Vacancy_Id, date(Sch.Date) as Date')
                      ->from('AssessmentSummery as Summ')
                      ->where(['Application_Id' => $Application_Id])
                      ->join('AssessmentSchedule as Sch','Sch.AssesementFor = Summ.Assessment_Type AND Sch.VacancyId = Summ.Vacancy_Id')
                      ->get()->result_array();
    } 

    public function Send_Letter($data){
      $procedure = $this->get_procedure($data['vacancy_Id'],1);
      if($procedure){
        $procedure = (Object)$procedure;
        $result = $this->db->get_where('AssessmentProcedure as ass',
                  "$procedure->Step between ass.Job_Grade_Low and jo.Job_Grade_High AND Step > $procedure->Step")
                  ->result_array();
                  
        if(count($result) > 0){
          return ['status'=>false,'message'=>'You Can not Send Letter. Because the Evaluation is not Completed.'];
        } else {
          $this->load->library('Message_Scheduler');
          $letter = $data['type'] == 'offer' ? ['Subject' => 'Job Offer Letter', 'Message' => ' Job Offer '] : ['Subject' => 'Regret Letter For Job Application', 'Message' => ' Regret '];
          
          $email_array = array();
          foreach($data['applicant'] as &$value){
            array_push($email_array, $value['Email']);
            unset($value['Email']);
          }

          $this->db->update_batch('Application',$data['applicant'],'id');
          $this->message_scheduler->schedule($letter['Subject'], $data['vacancy_Id'], json_encode($email_array, true), null);
          $this->db->update('Application', ['Hiring_Status' => 'Reserved', 'Status' => 'Failed'], 
          array('Hiring_Status is null', 'Status'=>'InProcess', 'VacancyId'=>$data['vacancy_Id']));

          if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return ['status'=>true,'message'=>'Job ' . $letter['Message'] . 'Letter Send Successfully For The Selected Applicant.'];
          } else {
            $this->db->trans_rollback();
            return ['status'=>false,'message'=>'Unable to Send '. $letter['Message'] .' Letter.'];
          }
        }
      }     
    }

    private function get_procedure($id, $step = 0){
      return $this->db->select('CONCAT(ass.Job_Grade_Low," - ",ass.Job_Grade_High) as JobGrade,Step,Assessment,
                                Internal_Min_Point,External_Min_Point,Affirmative_Min_Point,Vacancy.Title')
                      ->from('Vacancy as vac')
                      ->where('vac.id', $id)
                      ->join('Jobs as jo', 'jo.id = vac.JobId')
                      ->join('AssessmentProcedure as ass', 'jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High AND ass.Step = (vac.Current_Step - ' . $step. ')')
                      ->get()->row();
    }

    public function Get_Vacancies() {
      return $this->db->select('CONCAT(vac.Title," => ",date(Created_date)) As text,vac.id As value,vac.Created_date As date')
                      ->from('Vacancy as vac')
                      ->where('vac.Current_Step is not null')
                      ->join('Jobs as jo', 'jo.id = vac.JobId')
                      ->join('AssessmentProcedure as ass', 'jo.JobGrade between ass.Job_Grade_Low and ass.Job_Grade_High AND (ass.Step + 1) = vac.Current_Step')
                      ->get()->result_array();
    }

  } 