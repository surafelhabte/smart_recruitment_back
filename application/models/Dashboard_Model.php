<?php 
   Class Dashboard_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function GetDashboardData() {
      $dashboard = array();
      $dashboard['vacantPositions'] = count($this->db->get('Vacancy')->result_array());
      $dashboard['applications'] = count($this->db->get('Application')->result_array());
      $dashboard['screenings'] = count($this->db->get_where('Application', array('Shortlist'=>'First OR Second'))->result_array());
      $dashboard['passedCandidates'] = count($this->db->get_where('Application', array('Status'=>'Pass'))->result_array());
      $dashboard['offeredCandidates'] = count($this->db->get_where('Application', array('Hiring_Status'=>'Selected'))->result_array());
      $dashboard['reserved'] = count($this->db->get_where('Application', array('Hiring_Status'=>'Reserved'))->result_array());
      $dashboard['evaluations'] = count($this->db->group_by('Vacancy_Id')->from('AssessmentSummery')->get()->result_array());
      $dashboard['accepted'] = count($this->db->get_where('Application', array('Hiring_Status'=>'Hired'))->result_array());
      $dashboard['Recruitment_Request'] = count($this->db->get_where('RecruitmentRequest',
       array('Status'=>'Inprocess', 'TypeOfEmployment' => 'New'))->result_array());

      return $dashboard;
    }

    public function intialize_reports($data) {
      return $data;
    }
      
    public function save_report($data){
      $this->db->trans_begin();
      $this->db->set($data);
      if($this->db->insert('saved_reports'))
      {
        $this->db->trans_commit();          
        return ['status'=>true,'message'=>'Report Saved Successfully !'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Unable to Save Report.'];
      } 
    }
    
  } 