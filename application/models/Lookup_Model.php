<?php 
   Class Lookup_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function Create_Lookup($data) {
      $this->db->trans_begin();
      
      $data = json_decode($data, true);
      if($this->db->insert_batch('Lookups', $data))
      {
        $this->db->trans_commit();          
        return ['status'=>true,'message'=>'Lookup Created Successfully !'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Lookup Not Created'];
      } 
    }

    public function Update_Lookup($data) {
      $this->db->trans_begin();

      $this->db->where('id', $data['id']);
      $this->db->set($data);
     
      if($this->db->update('Lookups'))
      {
        $this->db->trans_commit();
        return ['status'=>true,'message'=>'Lookup updated successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'Lookup not updated.'];
      }
    } 
  
    public function Get_Lookups() {
      $this->db->order_by('Title', 'ASC');
      $result = $this->db->get('Lookups')->result_array();
      foreach($result as &$value){
        $value['Text'] = preg_replace('/(?<!\ )[A-Z]/',' $0', $value['Title']);
      }
      return $result;
    }

    public function Get_Lookup($id) {
      $result = $this->db->get_where('Lookups', array('id' => $id))->result_array();
      return $result;
    }

    public function Delete_Lookup($id) {
      $this->db->trans_begin();

        if($this->db->delete('Lookups', array('id' => $id)))
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'Lookup Deleted successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to Delete Lookup.'];
        }
    }        
     
    public function Load_Title_Data($value = null) {
      $this->db->distinct();
      $this->db->select('Title As text, Value AS value');
      $result = $this->db->get('Lookup_Titles')->result_array();
      return $result;
    }

    public function load_lookups($post) {
      $total = array();
      foreach($post as $field){
        $this->db->select('Value As text, Value AS value');
        $result = $this->db->get_where('Lookups',array('Title' => $field))->result_array();
        $total[$field] = $result;
      }
      return $total;
    }
  } 