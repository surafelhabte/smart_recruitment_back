<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class AssessmentSchedule extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
    }

    public function Create_post() {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Assessment_Schedule_Model->Create_Schedule($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }
  
    public function Update_post()
    {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Assessment_Schedule_Model->Update_Schedule($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetSchedules_get()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Assessment_Schedule_Model->Get_Schedules();
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetSchedule_get($id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Assessment_Schedule_Model->Get_Schedule($id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function Delete_delete($id) {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Assessment_Schedule_Model->Delete_Schedule($id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetCurrentStep_post()
    {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
           $result = $this->Assessment_Schedule_Model->Get_Current_Step($post['vacancyId']);
           $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function CheckPanelMembers_post()
    {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());

        if($response){
           $result = $this->Assessment_Schedule_Model->Check_Panel_Members($post);
           $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
        
    }


     
}
