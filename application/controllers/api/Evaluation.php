<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Evaluation extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
    }

    public function LoadApplications_get($vacancy_Id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Evaluation_Model->Load_Applications($vacancy_Id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function SaveResult_post()
    {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Evaluation_Model->Save_Result($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetVacancies_get()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Evaluation_Model->Get_Vacancies();
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function GetPanelMembers_post(){
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Evaluation_Model->Get_Panel_Members($this->post());
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

}
