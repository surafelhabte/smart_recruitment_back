<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Tokenizer extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
        $this->core_Db=config_item('core_db');

    }

    public function Get_token_get()
    {
        $token = AUTHORIZATION::generateToken(rand());
        $this->response((Object)['token' => $token], REST::HTTP_OK);
    }

    public function Get_identity_post()
    {
        $post = $this->post();
        $result = $this->db->select(', emp.is_department_head,dep.id as dep_id,pos.position')
                            ->from("$this->core_Db.employee_data as emp")
                            ->where(['emp.employee_id'=>$post['id']])
                            ->join("$this->core_Db.position as pos","emp.position_id = pos.id")
                            ->join("$this->core_Db.department as dep","pos.department_id = dep.id")
                            ->get()->row();

        $result->allow_screen = true;                    
        
        $this->response($result, REST::HTTP_OK);
    }

}
