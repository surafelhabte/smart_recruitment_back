<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Report extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
    }

    public function intialize_post()
    {       
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Report_Model->intialize_reports($this->post());
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function Process_Request_post()
    {      
       $post = $this->post();
       $this->response($post, REST::HTTP_OK);
    }

    public function Save_Report_post()
    {      
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Report_Model->Save_Report($this->post());
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function Get_Saved_Reports_get()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Report_Model->Get_Saved_Reports();
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function Update_Saved_Report_post()
    {
        $post = $this->post();       
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Report_Model->Update_Saved_Report($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function Delete_Saved_Report_delete($id) {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Report_Model->Delete_Saved_Report($id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

} 


