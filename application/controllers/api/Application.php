<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Application extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
    }

    public function Update_post()
    {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Update_Application($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetApplicationsFirst_get($vacancy_id)
    {        
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Get_Applications_First($vacancy_id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetApplicationsSecond_post()
    {        
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Get_Applications_Second($this->post());
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetApplication_get($application_id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Get_Application($application_id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function Get_First_Shortlisted_get($vacancy_id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Get_First_Shortlisted_Applications($vacancy_id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetVacancies_get()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Get_Vacancies();
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function GetComments_get($id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Get_Comments($id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function PostComment_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Application_Model->Post_Comment($this->post());
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

}
