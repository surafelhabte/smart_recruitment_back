<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Message_Scheduler {
    public function __construct(){
        
    }

    public function schedule ($Subject, $Body,$For, $schedule_id = null){
        $CI =& get_instance();
        $data = array('Subject' => $Subject, 'Body' => $Body, 'For' => $For, 'Message_For_id' => $schedule_id === null? NULL: $schedule_id);
        $CI->db->trans_begin();
        $CI->db->set($data);
 
        if($CI->db->insert('Message_Scheduler')){
            $CI->db->trans_commit();
            return true;
        } else {
            $CI->db->trans_rollback();
            return false;
        }
    }    
}