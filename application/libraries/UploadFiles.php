<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UploadFiles {
    
    public function __construct(){}

    public function uploadFile($parameter)
    {
        $field;
        $config = array();
        $data = array();

        $CI =& get_instance();

        $field = $parameter['field'];
        $config['upload_path']  = $parameter['uploadlocation'];
        $config['allowed_types'] = $parameter['allowedExtenstion'];
        $config['max_size']  = $parameter['allowedSize'];
        $config['file_name'] = time();
        $data=[];

        if (!is_dir($parameter['uploadlocation'])) {
            mkdir($parameter['uploadlocation'], 0777, true);
        }

        $CI->load->library('upload');
      
        $CI->upload->initialize($config);
        if (!$CI->upload->do_upload($parameter['field']))
        {
            $error[] = array('error' => $CI->upload->display_errors());
            return [];
        }
        else
        {
            $data[$parameter['field']] = $CI->upload->data();
        }
        return $data;
    }

    public function deleteFile($path)
    {
        if (unlink($path)) {
            return true;
        } else {
            return false;
        }
    }
}